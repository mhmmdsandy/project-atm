#include <iostream>
#include <stdlib.h>
#include <string>
using namespace std;
class Bank {
private:
	string name;
	int accnumber;
	char type[10];
	int amount = 0;
	int tot = 0;

	// Public variables
public:
	// Function to set the person's data
	void setvalue()
	{
		cout << "Enter your name\n";
		cin.ignore();
		// To use space in string
		getline(cin, name);
		cout << "Balance\n" << tot << endl ;
	}

	// Function to deposit the amount in ATM
	void deposit()
	{
		cin >> amount;
        cout << "\nYour balance\n" << amount << endl;
        tot = tot + amount;
		cout << "\nTotal balance is: " << tot << endl;
	}

	// Function to show the balance amount
	void showbal()
	{
		tot = tot + amount;
		cout << "\nTotal balance is: " << tot;
	}

	// Function to withdraw the amount in ATM
	void withdrawl()
	{
		int a, avai_balance;
		cin >> a;
		if(a > tot){
		    cout << "Your Balance only " << tot << endl;
		}else{
		    avai_balance = tot - a;		    
		    cout << "Your Balance is " << avai_balance << endl;
		}
	}
};

// Driver Code
int main()
{
	// Object of class
	Bank b;
	int choice;

	// Infinite while loop to choose
	// options everytime
	while (1) {
		cout << "Enter Your Choice\n";
		cout << "\t1. login\n";
		cout << "\t2. deposit\n";
		cout << "\t3. withdraw\n";
		cout << "\t4. cancel\n";
		cin >> choice;

		// Choices to select from
		switch (choice) {
		case 1:
			b.setvalue();
			break;
		case 2:
			b.deposit();
			break;
		case 3:
			b.withdrawl();
			break;
		case 4:
			exit(1);
			break;
		default:
			cout << "\nSorry, your option not available\n";
		}
	}
}